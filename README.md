Task: [keyname]

- When to update [declare hooks]
- Update function
- Schema
- Name
- Display scripts
- API return scripts
- Rest Methods to get/set/delete/create the value
- Collection type [repo/branch/global]
- init method
- update method
- cli command and callback


Entry points:
cd repo ->
  gofer init ->
    - cli script that parses argv and accepts 'init' command
    - init script that:
      - stores repo path
      - opens the repo with nodegit
      - grabs all relevant metadata from the repo
        - repo path
        - branch name
        - (perhaps this can be registered by tasks as well)
      - initializes a model with it, and saves it
      - sets core.hooksPath in repo to internal hooks dir
      - runs init method for all tasks
      - NeXT: Init DB and repos Collection.
      - Next: Setup/Configure DB connection
    get repo path ->
      get repo metadata ->
        create record in db for repo ->
          configure repo for gofer (hooks) ->
          run init tasks ->

git hook ->
  get repo/branch metadata from db ->
    look up registry for tasks to run on hook ->
      run registered tasks ->

gofer cli ->
  devise tasks ->
    run task ->


