function setConfigPath(config, key, val) {
  return config.setString(key, val);
}

module.exports = {
  setConfigPath
};
