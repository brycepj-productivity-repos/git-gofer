function getBranchName(branch) {
  return branch.name();
}

module.exports = {
  getBranchName
};
