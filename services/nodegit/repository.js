const Repository = require('nodegit').Repository;

function isSameRepo(repo, oid) {
  return new Promise((resolve, reject) => {
    repo.getCommit(oid)
      .then(resolve(true))
      .catch(resolve(false));
  });
}

function getMasterCommit(repo, sha) {
    return repo.getMasterCommit()
}

function getCommitOid (repo, commit) {
  return commit.id();
}

function getCurrentRepo(cwd) {
  return Repository.open(cwd);
}

function getRepoConfig(repo) {
  return repo.config();
}

function getCurrentBranch(repo) {
  return repo.getCurrentBranch()
    .then(getBranchName);
}

function setHooksPath(repo) {
  return repo.config()
    .then(setHooksPath)
    .then(() => repo);
}

module.exports = {
  getCurrentRepo,
  getRepoConfig,
  getCurrentBranch,
//  setConfigHooksPath,
//  isSameRepo,
//  getMasterCommit,
// Rep  getCommitOid
};
