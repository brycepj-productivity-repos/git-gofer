const path = require('path');

const project_root_dir = global.project_root_dir;
const internal_hooks_dir = path.join(project_root_dir, 'hooks');

module.exports = {
  project_root_dir,
  internal_hooks_dir
};
