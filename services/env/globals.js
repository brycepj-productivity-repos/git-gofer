const path = require('path');

global.$require = (local_path) => {
  const absolute_path = path.join(__dirname, '../..', local_path);
  return require(absolute_path);
};
