// db_init().then(log.success('Successfully initialized the database'));

function errorAsync(message) {
  return logMaker('error', message);
}

function logAsync(method, message) {
  return () => {
    return logBase(method, message, opts);
  };
}

function logSync(method, message, opts) {
  logBase(method, message, opts);
}

function logBase(method, message, opts) {
  return base_loggers[method](message);
}

// in each of these configure colors, log paths, stdout, etc -- anything worth displaying
const base_loggers = {
  info(message) {
    // bunyan_instance.loginfo... (colors, etc)
  },
  warn(message) {
    // bunyan_instance.loginfo... (colors, etc)
  },
  error(message) {
    // bunyan_instance.loginfo... (colors, etc)
  },
  success(message) {
    // bunyan_instance.loginfo... (colors, etc)
  },
};

module.exports = {
  error: errorAsync,
  errorSync: errorSync,
  success: successAsync,
  successSync: successSync,
  info: infoAsync,
  infoSync: infoSync,
  warn: warnAsync,
  warnSync: warnSync
};
