const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema_definition = {
  repo_path: { type: String, required: true },
//  current_branch
//  base_commit: { type: String, require: true },
//  remotes: []
//  branches: []
};

const schema_options = {
  id: true,
  strict: true,
  getters: true,
  validateBeforeSave: true,
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
};

const RepoSchema = new Schema(schema_definition, schema_options);

// one public method per key that needs saving. Rely on the services to do the rest
RepoSchema.methods.setBaseCommit = () => {
  return git.getBaseCommit().then((base_commit_oid) => {
    this.base_commit = base_commit_oid;
  });
};

// These will be called from db/api which will be called from commands api -- remember, they are only the data layer
RepoSchema.methods.setRemotes = () => {
  return git.getRepoRemotes();
};

RepoSchema.methods.addBranch = () => {
  return git.getBranchInfo().then(() => {
    this.branches.push({});
  });
};

RepoSchema.methods.setCurrentBranch = () => {
  return git.getCurrentBranchRef().then((ref) => {
    this.current_branch = ref;
  })
};

RepoSchema.methods.onCheckout = () => {
  // update current branch and init if neccessary
};

// Instance methods can operate on this ^^ better than adding all of these to the DB API
// Statics work nicely as well, these can be where the queries are defined.
// Virtuals are non persistent getters and setters. Basically proxies

module.exports = mongoose.model('Repo', RepoSchema);
