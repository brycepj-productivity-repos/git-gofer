var mongoose = require('mongoose');

const db_options = 'mongodb://localhost/git-gofer';

module.exports = (callback) => {
  mongoose.connect(db_options, callback)
};
