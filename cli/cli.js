#!/usr/bin/env node

require('../services/env/globals');

const cli_app = global.__CLI_APP__ = require('commander');

cli_app
  .version('0.0.1')
  .command('init')
  .description('initialize the current repo and branch')
  .action((cmd) => $require('cli/commands/init')(process.cwd()));

  // .command('print', 'print everything gofer knows about the repo and branch')
cli_app.parse(process.argv);
