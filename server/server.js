#!/usr/bin/env node

var restify = require('restify');

const init_tasks = require('./init_task_routes');

function respond(req, res, next) {
  res.send('hello ' + req.params.name);
  next();
}

var server = global.__REST_SERVER__ = restify.createServer();

init_tasks();

server.get('/hello/:name', respond);
server.head('/hello/:name', respond);

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});
