function requireTask(key) {
  return $require(`tasks/${key}/setup`);
}

module.exports = () => {
  requireTask('merge_request_url');
};
