### Gofer [goh-fer]

**noun, Slang.** An employee whose chief duty is running errands.

### Use cases
- Store and display MR (on many hooks?)
- Run tests in a new window
- More verbosity on Git commands (e.g. Meta data)
- Extend git with more than aliases
- Custom githooks

#### Plan

- Wrap git() everywhere.
  - When git is run, check the db to see if it's been initialized.
  - If not, immediately pass and let git do its thing
  - If it has been, intercept the command, parse it, pass to your own hooks, *then* pass to git whenever you're ready
  - This gives you full control, over pre, post, and during every git command.

There will be a githook manager, with an internal and external API. It can be registered and deregistered.

Potential Custom Githooks:
- Delete branch
- Decorate branch list
- Guidelines about commit messages
- Better Git syntax coloring?

Existing Githooks:
  applypatch-msg
  pre-applypatch
  post-applypatch
  pre-commit
  prepare-commit-msg
  commit-msg
  post-commit
  pre-rebase
  post-checkout
  post-merge
  pre-receive
  update
  post-receive
  post-update
  pre-auto-gc
  post-rewrite
  pre-push

### Moving parts / dependencies

For API inspiration, look at: https://github.com/brigade/overcommit
The Public CLI - parses direct input (e.g. `gofer store`) - Look at Vorpal --> https://github.com/dthree/vorpal
The Git CLI - parses git input --> Vorpal -->  https://github.com/dthree/vorpal
The Githook manager - API for getting/setting/updating/deleting existing and custom githooks --> look to overcommit for API ideas
The Repo Bridge - Logic that can interface with the current repo --> Nodegit
The Repo Data Store - API for getting/setting/updating/deleting existing and custom metadata about each repo/branch -> for API ideas https://github.com/mafintosh/flat-file-db
The Plugin manager - API for registering, configuring and deregistering custom plugins

Always ask, what will be the easiest way to consume this class/method/API?














