// called when hooks are triggered.
// should make use of save/create method in rest_api
// and return the promise

const save = require('./rest_api').save;

module.exports = () => {
  // make request to gitlab api
  // parse response
  // isolate merge request url
  return save(url);
};
