const schema = mongoose..
const server = $require('services/rest_server');
const cli = $require('services/cli');

function update() {

}

function cliApi() {
  // define commands/actions/options for display
}

function restApi() {
  // define endpoint/params/methods for accessing merge_request_url
}

module.exports = {
  key: 'merge_request_url',
  update,
  schema,
  hooks: ['pre-commit', 'pre-push'],
  expose: [ cliApi, restApi ]
};
