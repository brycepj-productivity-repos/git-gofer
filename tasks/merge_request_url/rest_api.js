// each should use the schema to interact with the database, returning the promise

function get() {

}

function save() {

}

function update() {

}

function remove() {

}

module.exports = {
  get,
  save,
  update,
  remove
}
